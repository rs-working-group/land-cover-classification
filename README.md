# Crop Vs Non Crop Classification

This repo shows you how to perform supervised classification.The Classifier 
package handles supervised classification using Earth Engine resources. 
The general workflow for classification is:

0. Environment Setup
1. Collect training data. Assemble features which have a property that
stores the known class label and properties storing numeric values for the predictors.
2. Instantiate a classifier. Set its parameters if necessary.
3. Train the classifier using the training data.
4. Classify an image or feature collection.
5. Estimate classification errors with independent validation data.
6. Generate raster products from of aoi given date windows
7. Generate estimated pixel areas per class classified

To run the classification, you will have to:

### For Hot Dry Season

1. Change these paths to your file directories

study_site_path = "projects/ee-hubertkanye/assets/SRV" and
crop_noncrop = "projects/ee-hubertkanye/assets/crop_noncrop_new"

And you can download the training Sample here:
[crop vs noncrop training HDS](https://drive.google.com/drive/folders/1W2VrxKe8TczoIwDe_kFdTRp8uZVAWhPJ?usp=share_link)

2. Excute the following script:
python crop_noncrop_classification_hot_dry.py

### For Rainy Season

1. Change these paths to your file directories

study_site_path = "projects/ee-hubertkanye/assets/SRV" and
crop_noncrop = "projects/ee-hubertkanye/assets/crop_noncrop_rainy_season"

And you can download the training Sample here:
[crop vs noncrop training RAINY](https://drive.google.com/drive/folders/131vMysJC1sunkVPgLJBWLytjz4r6fkof?usp=share_link)

2. Excute the following script:
python crop_noncrop_classification_rainy.py

3. Here are the GEE scripts per region

* [GEE link for Dagana](https://code.earthengine.google.com/c68910f65549ec05acb7d7a62c027307)

* [GEE link for Podor](https://code.earthengine.google.com/54e2e524b15d4fb59f01be324961153c)

* [GEE link for Matam](https://code.earthengine.google.com/6d8f19559fe49020bf1266527c018917)


### For Cold Dry Season

1. Change these paths to your file directories

study_site_path = "projects/ee-hubertkanye/assets/SRV" and
crop_noncrop = "projects/ee-hubertkanye/assets/crop_noncrop_training_CDS"

And you can download the training Sample here:
[crop vs noncrop training CDS](https://drive.google.com/drive/folders/1Yyob7Pl-1e0exbJx67t4bKvXMuI-_aqG?usp=share_link)

2. Excute the following script:
python crop_noncrop_classification_cold_dry.py

3. Here are the GEE scripts per region

* [GEE link for Dagana](https://code.earthengine.google.com/402401a137f6cbaf3e3853b0063af26a)

* [GEE link for Podor](https://code.earthengine.google.com/b9bae1a7e57276c87809505623633095)

* [GEE link for Matam](https://code.earthengine.google.com/0f02568dc78ad7a1f7d01f0b9723406f)